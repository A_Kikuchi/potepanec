require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "Application helpers Title method" do
    it "タイトルが生成されること" do
      expect(full_title(page_title: "hoge")).to eq "hoge - BIGBAG Store"
      expect(full_title(page_title: "")).to eq "BIGBAG Store"
      expect(full_title(page_title: nil)).to eq "BIGBAG Store"
      expect(full_title).to eq "BIGBAG Store"
    end
  end
end
