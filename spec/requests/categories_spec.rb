require "rails_helper"

RSpec.describe "Categories", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, name: "bags", parent: taxonomy.root) }
    let(:other_taxon) { create(:taxon, name: "shirts", parent: taxonomy.root) }
    let!(:product) { create(:product, name: "hoge_bag", taxons: [taxon]) }
    let(:other_product) { create(:product, name: "foo_shirt", taxons: [other_taxon]) }

    before do
      get potepan_category_url(taxon.id)
    end

    it "HTTPレスポンスがステータスコード200を返すこと" do
      expect(response.status).to eq 200
    end

    it "taxon名が表示されること" do
      expect(response.body).to include taxon.name
    end

    it "product名が表示されること" do
      expect(response.body).to include product.name
    end

    it "other_productsが表示されないこと" do
      expect(response.body).not_to include other_product.name
    end
  end
end
