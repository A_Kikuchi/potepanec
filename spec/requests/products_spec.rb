require "rails_helper"

RSpec.describe "Products", type: :request do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get potepan_product_url(product.id)
    end

    it "HTTPレスポンスがステータスコード200を返すこと" do
      expect(response).to have_http_status 200
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "価格が表示されること" do
      expect(response.body).to include product.price.to_s
    end

    it "商品情報が表示されること" do
      expect(response.body).to include product.description
    end
  end
end
