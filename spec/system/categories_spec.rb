require "rails_helper"

RSpec.describe "Categories", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: "bags", parent: taxonomy.root) }
  let!(:other_taxon) { create(:taxon, name: "shirts", parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "タイトルが表示される" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
  end

  it "商品名をクリックすると商品詳細ページに遷移する" do
    expect(page).to have_content product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "商品価格をクリックすると商品詳細ページに遷移する" do
    expect(page).to have_content product.display_price
    click_link product.display_price
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "サイドバーのカテゴリーをクリックするとカテゴリーページに遷移する" do
    within ".sideBar" do
      expect(page).to have_selector('li', text: taxonomy.name)
      expect(page).to have_selector('li', text: other_taxon.name)
      click_link other_taxon.name
      expect(current_path).to eq potepan_category_path(other_taxon.id)
    end
  end
end
