require "rails_helper"

RSpec.describe "Products", type: :system do
  let(:product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  it "タイトル、商品名、価格、商品情報が表示される" do
    aggregate_failures do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  it "ヘッダーのロゴがクリックされたらトップページに遷移する" do
    within(:css, ".header") do
      click_on "logo"
    end
    expect(page).to have_current_path potepan_path
  end

  it "LightSectionのHomeリンクがクリックされたらトップページに遷移する" do
    within(:css, ".lightSection") do
      click_link "Home"
    end
    expect(page).to have_current_path potepan_path
  end

  it "ヘッダーのHomeリンクがクリックされたらトップページに遷移する" do
    within(:css, ".header") do
      click_link "Home"
    end
    expect(page).to have_current_path potepan_path
  end
end
